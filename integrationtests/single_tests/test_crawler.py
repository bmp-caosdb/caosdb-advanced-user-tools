#!/usr/bin/env python
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
import unittest
from copy import deepcopy
from tempfile import NamedTemporaryFile

import caosdb as db

from caosadvancedtools.crawler import Crawler


def seek_and_destroy(names):
    for name in names:
        db.execute_query("FIND "+name).delete(raise_exception_on_error=False)


class CrawlerTest(unittest.TestCase):
    def setUp(self):
        # TODO replace by something more reasonable
        seek_and_destroy(["Experiment", "Analysis", "Publication", "species"])
        self.rts = db.Container().extend([
            db.RecordType(name="Experiment").insert(),
            db.RecordType(name="Analysis").insert(),
            db.RecordType(name="Publication").insert(),
            db.Property(name="species", datatype=db.TEXT).insert(),
        ])
        self.exp = db.Record()
        self.exp.add_parent(name="Experiment")
        self.exp.add_property(name="species", value="microunicorn")
        self.ana = db.Record()
        self.ana.add_parent(name="Analysis")
        self.pub = db.Record()
        self.pub.add_parent(name="Publication")

    def test_check_existence(self):
        assert Crawler.find_existing(self.exp) is None

    def test_find_or_insert_identifiables(self):
        tmpexp = db.Record()
        tmpexp.add_parent(name="Experiment")
        tmpexp.add_property(name="species", value="microunicorn")
        tmpana = db.Record()
        tmpana.add_parent(name="Analysis")
        tmpexp.insert()
        tmpana.insert()
        self.ana.id = tmpana.id
        # exp inserted/no id; ana inserted/id; pub missing
        identifiables = db.Container().extend([self.exp, self.ana, self.pub])
        old_id = id(identifiables[0])
        reference_to_first = identifiables[0]
        assert reference_to_first is identifiables[0]
        Crawler.find_or_insert_identifiables(identifiables)

        for el in identifiables:
            assert el.is_valid()

        # check whether instance is the same
        assert reference_to_first is identifiables[0]
        assert old_id == id(identifiables[0])
        # order must not be changed
        assert identifiables[0].get_parents()[0].name == "Experiment"
        assert identifiables[1].get_parents()[0].name == "Analysis"
        assert identifiables[2].get_parents()[0].name == "Publication"

    def tearDown(self):
        for el in [self.exp, self.ana, self.pub, self.rts]:
            try:
                el.delete()
            except:
                pass


class CrawlerTestExist(CrawlerTest):
    def setUp(self):
        super().setUp()
        self.exp.insert()
        self.ana.insert()
        self.pub.insert()

    def test_check_existence(self):
        res = Crawler.find_existing(self.exp)
        assert res.id == self.exp.id

    def tearDown(self):
        for el in [self.exp, self.ana, self.pub, self.rts]:
            try:
                el.delete()
            except:
                pass
