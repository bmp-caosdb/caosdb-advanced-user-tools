#!/usr/bin/env python3
from caosmodels.parser import parse_model_from_yaml

model = parse_model_from_yaml("model.yml")
model.sync_data_model(noquestion=True)
