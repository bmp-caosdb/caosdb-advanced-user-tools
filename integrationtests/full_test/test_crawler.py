#!/usr/bin/env python3
import os
import unittest

import caosdb as db


def get_entity_with_id(eid):
    return db.execute_query("FIND "+str(eid), unique=True)


class CrawlerTest(unittest.TestCase):
    def test_experiment(self):
        ########################
        # # first experiment # #
        ########################
        # TODO saving an empty string as value in a text property leads to a
        # vanishing of the property
        # thus an x is used here. Needs to be fixed.
        exp = db.execute_query(
            "FIND Experiment with date=2019-02-03 and identifier='x'",
            unique=True)

        # There should be a Project with name TestProject which is referenced
        project_id = exp.get_property("Project").value
        project = get_entity_with_id(project_id)
        assert project.name == "TestProject"
        assert "Project" in [p.name for p in project.get_parents()]

        # There should be a datafile as result attached with path datafile.dat
        datfile_id = exp.get_property("results").value[0]
        datfile = get_entity_with_id(datfile_id)
        assert os.path.basename(datfile.path) == "datafile.dat"

        #########################
        # # second experiment # #
        #########################
        exp = db.execute_query(
            "FIND Experiment with date=2019-02-03 and identifier='something'",
            unique=True)

        # Should be the same project
        assert project_id == exp.get_property("Project").value

        # Should have two data files
        datfile_ids = exp.get_property("results").value
        datafile_names = []

        for did in datfile_ids:
            datfile = get_entity_with_id(did)
            datafile_names.append(os.path.basename(datfile.path))
        assert "usefull.xlsx" in datafile_names
        assert "useless.xlsx" in datafile_names

    def test_analysis(self):
        ######################
        # # first analysis # #
        ######################
        ana = db.execute_query(
            "FIND Analysis with date=2019-02-03 and identifier='x'",
            unique=True)

        # There should be a Project with name TestProject which is referenced
        project_id = ana.get_property("Project").value
        project = get_entity_with_id(project_id)
        assert "Project" in [p.name for p in project.get_parents()]

        # There should be a file as result attached with path results.pdf
        datfile_id = ana.get_property("results").value[0]
        datfile = get_entity_with_id(datfile_id)
        assert os.path.basename(datfile.path) == "results.pdf"

        # There should be a file as script attached with path plot.py
        datfile_id = ana.get_property("scripts").value[0]
        datfile = get_entity_with_id(datfile_id)
        assert os.path.basename(datfile.path) == "plot.py"

        #######################
        # # second analysis # #
        #######################
        ana = db.execute_query(
            "FIND Analysis with date=2019-02-03 and identifier='something'",
            unique=True)

        # Should be the same project
        assert project_id == ana.get_property("Project").value

        # Should have two data files
        datfile_ids = ana.get_property("results").value
        datafile_names = []

        for did in datfile_ids:
            datfile = get_entity_with_id(did)
            datafile_names.append(os.path.basename(datfile.path))
        assert "lol1.png" in datafile_names
        assert "lol2.png" in datafile_names

        # There should be a file as script attached with path plot.py
        datfile_id = ana.get_property("scripts").value[0]
        datfile = get_entity_with_id(datfile_id)
        assert os.path.basename(datfile.path) == "analyse.py"

    def test_publication(self):
        #########################
        # # first publication # #
        #########################
        pub = db.execute_query("FIND *really_cool_finding", unique=True)

        # There should be a Project with name TestProject which is referenced

        ##########################
        # # second publication # #
        ##########################
        pub = db.execute_query("FIND *paper_on_exciting_stuff ", unique=True)
