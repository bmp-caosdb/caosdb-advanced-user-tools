#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import argparse
from argparse import RawTextHelpFormatter

import caosdb as db

from caosadvancedtools.crawler import Crawler
from scifolder import (AnalysisCFood, ExperimentCFood, PublicationCFood,
                       SimulationCFood, ProjectCFood)


def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=RawTextHelpFormatter)

    parser.add_argument("path")

    return parser


def access(path):
    return "extroot" + path


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    print("Starting query...")
    files = Crawler.query_files(args.path)
    print("Query done...")
    config = db.configuration.get_config()
    c = Crawler(use_cache=True, access=access,
                food=[ProjectCFood, AnalysisCFood, ExperimentCFood,
                      PublicationCFood, SimulationCFood, ])
    c.crawl(files)
