---
responsible:	
- Only Responsible
description: 	A description of another example experiment.

results:
- filename:	"/ExperimentalData/TestProject/2019-02-03/*.dat"
  description:  an example reference to a results file
...
