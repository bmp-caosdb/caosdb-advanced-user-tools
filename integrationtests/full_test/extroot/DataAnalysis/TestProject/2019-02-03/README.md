---
responsible:	
- Only Responsible
description: 	A description of another example analysis.

data:
- filename:	"/ExperimentalData/TestProject/2019-02-03/*.dat"
  description:  an example reference to a results file

scripts:
- filename: plot.py
  description: a plotting script
results:
- filename: results.pdf
...
