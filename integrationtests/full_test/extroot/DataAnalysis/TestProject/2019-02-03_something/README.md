---
responsible:	
- First Person
description: 	A description of an example analysis.

results:
- filename:	"images/*.png"
  description:  an example reference to a results file

data:
- filename: /ExperimentalData/TestProject/2019-02-03_something/use*.xlsx
  description:  an example reference to data files

scripts:
- analyse.py

tags:
- collagen
- time sweep
- frequency sweep
...
