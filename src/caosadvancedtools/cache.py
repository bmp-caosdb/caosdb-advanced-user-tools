
# TODO this is implementing a cache on client side. Should it be on
# server side?
import os
import sqlite3
from hashlib import sha256

import caosdb as db
from lxml import etree


def get_pretty_xml(cont):
    if isinstance(cont, list):
        cont = db.Container().extend(cont)

    if not isinstance(cont, db.Container):
        cont = db.Container().append(cont)

    return etree.tounicode(cont.to_xml(
        local_serialization=True), pretty_print=True)


class Cache(object):
    def __init__(self, db_file=None):
        if db_file is None:
            self.db_file = "cache.db"
        else:
            self.db_file = db_file

        if not os.path.exists(self.db_file):
            self.create_cache()

    def create_cache(self):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.execute('''CREATE TABLE identifiables (digest text primary key, caosdb_id integer)''')
        conn.commit()
        conn.close()

    def hash_entity(ent):
        xml = get_pretty_xml(ent)
        digest = sha256(xml.encode("utf-8")).hexdigest()

        return digest

    def insert(self, ent_hash, ent_id):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.execute('''INSERT INTO identifiables VALUES (?, ?)''',
                  (ent_hash, ent_id))
        conn.commit()
        conn.close()

    def check_existing(self, ent_hash):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.execute('''Select  * FROM identifiables WHERE digest=?''',
                  (ent_hash,))
        res = c.fetchone()
        conn.commit()
        conn.close()

        if res is None:
            return res
        else:
            return res[1]

    def update_ids_from_cache(self, entities):
        """ sets ids of those entities that are in cache

        A list of hashes corresponding to the entities is returned
        """
        hashes = []

        for ent in entities:
            ehash = Cache.hash_entity(ent)
            hashes.append(ehash)
            eid = self.check_existing(ehash)

            if eid is not None:
                ent.id = eid

        return hashes

    def insert_list(self, hashes, entities):
        """ Insert the ids of entities into the cache

        The hashes must correspond to the entities in the list
        """

        for ehash, ent in zip(hashes, entities):
            if self.check_existing(ehash) is None:
                self.insert(ehash, ent.id)
