#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import os

import caosdb as db


def replace_path_prefix(path, old_prefix, new_prefix):
    """
    Replaces the prefix old_prefix in path with new_prefix.

    Raises a RuntimeError when the path does not start with old_prefix.
    """

    if not path.startswith(old_prefix):
        raise RuntimeError(
            "Path does not start with old_prefix\n{}\nvs\n{}".format(
                path,
                old_prefix))
    path = path[len(old_prefix):]

    return os.path.join(new_prefix, path)


def string_to_person(person):
    """
    Creates a Person Record from a string.

    Currently only the format <Firstname> <Lastname> <*> is supported.
    """
    firstname = person.split(" ")[0]
    lastname = person.split(" ")[1]
    pr = db.Record()
    pr.add_parent("Person")
    pr.add_property("lastname", lastname)
    pr.add_property("firstname", firstname)

    return pr


def read_field_as_list(field):
    """
    E.g. in yaml headers entries can be single values or list. To simplify the
    work with those values, this function puts single values in a list.
    """

    if isinstance(field, list):
        return field
    else:
        return [field]


def find_file_included_by(glob):
    """
    Executes a query that looks for files included by a glob. The glob needs
    to be according to CaosDB rules.

    Returns a container.
    """

    query_string = "FIND file which is stored at {}".format(glob)

    try:
        return db.execute_query(query_string)
    except Exception as e:
        print(e)
        return []


def assure_absolute_path_in_glob(glob, prefix):
    """
    Prefixes a relative globs with some path.

    Some times files are defined by a relative glob (e.g. "scripts/**"). In
    order to search for such files in CaosDB, these globs are prefixed with the
    current location.

    A relative glob is identified by a missing "/" in the beginning.
    """

    if not glob.startswith("/"):
        glob = os.path.normpath(os.path.join(prefix, glob))
    else:
        glob = os.path.normpath(glob)

    return glob


def return_field_or_property(value, prop=None):
    """
    returns value itself of a property.

    Typical in yaml headers is that a field might sometimes contain a single
    value and other times a dict itself. This function either returns the
    single value or (in case of dict as value) a value of the dict.
    """

    if isinstance(value, dict) and prop in value:
        return value[prop]
    else:
        return value


def find_records_that_reference_ids(referenced_ids, rt="", step_size=50):
    """ Returns a list with ids of records that reference entities with
    supplied ids

    Sometimes a file or folder will be referenced in a README.md (e.g. in an
    Analysis) but not those files shall be referenced but the corresponding
    object  (e.g. the Experiment). Thus the ids of all Records (of a suitable
    type) are collected that reference one or more of the supplied ids.
    This is done in chunks as the ids are passed in the header of the http
    request.
    """
    record_ids = set()
    index = 0

    while index < len(referenced_ids):
        subset = referenced_ids[index:min(
            index+step_size, len(referenced_ids))]
        try:
            q_string = ("FIND Record {} which references \n".format(rt)
                        + " or which references \n".join(
                            [str(el) for el in subset]))
            exps = db.execute_query(q_string)
            record_ids.update([exp.id for exp in exps])
        except Exception as e:
            print(e)
            pass

        index += step_size

    return list(record_ids)
