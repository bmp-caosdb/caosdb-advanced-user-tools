#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
""" Defines how something that shall be inserted into CaosDB is treated.

CaosDB can automatically be filled with Records based on some file structure.
The Crawler will iterate over the files and test for each file whether a CFood
exists that matches the file path. If one does, it is instanciated to treat the
match. This occurs in basically three steps:
1. create a list of identifiables, i.e. unique representation of CaosDB Records
(such as an experiment belonging to a project and a date/time)
2. the identifiables are either found in CaosDB or they are created.
3. the identifiables are update based on the date in the file structure
"""

import re

import caosdb as db

ENTITIES = {}


def get_entity(name):
    """ Returns the entity with a given name, preferably from a local cache.

    If the local cache does not contain the entity, retrieve it from CaosDB.
    """

    if name not in ENTITIES:
        ent = db.Entity(name=name)
        ent.retrieve()
        ENTITIES[name] = ent

    return ENTITIES[name]


class AbstractCFood(object):
    # contains the compiled regular expression after the first execution of the
    # function match()
    _pattern = None

    def __init__(self, crawled_file, access=lambda x: x):
        """ Abstract base class for Crawler food (CFood).

        Parameters
        ----------
        crawled_file : The file that the crawler is currently matching. Its
                       path should match against the pattern of this class

        access : callable, optional
                 A function that takes a CaosDB path and returns a local path
        """
        self.access = access
        self.crawled_file = crawled_file
        self.crawled_path = crawled_file.path
        self.match = type(self).match(crawled_file.path)
        self.to_be_updated = db.Container()
        self.identifiables = db.Container()

    @staticmethod
    def get_re():
        """ Returns the regular expression used to identify files that shall be
        processed

        This function shall be implemented by subclasses.
        """
        raise NotImplementedError()

    @classmethod
    def match(cls, string):
        """ Matches the regular expression of this class against file names

        Parameters
        ----------
        string : str
                 The path of the file that shall be matched.
        """

        if cls._pattern is None:
            cls._pattern = re.compile(cls.get_re())

        return cls._pattern.match(string)

    def create_identifiables(self):
        """
        should set the instance variable Container with the identifiables
        """
        raise NotImplementedError()

    def update_identifiables(self):
        """ Changes the identifiables as needed and adds changed identifiables
        to self.to_be_updated
        """
        raise NotImplementedError()

    def push_identifiables_to_CaosDB(self):
        """ Updates the self.to_be_updated Container, i.e. pushes the changes
        to CaosDB
        """

        if len(self.to_be_updated) == 0:
            return
        get_ids_for_entities_with_names(self.to_be_updated)
        self.to_be_updated.update()

    @staticmethod
    # move to api?
    def set_parents(entity, names):
        entity.parents.clear()

        for n in names:
            entity.add_parent(get_entity(n))

    @staticmethod
    # move to api?
    def remove_property(entity, prop):
        # TODO only do something when it is necessary?

        if isinstance(prop, db.Entity):
            name = prop.name
        else:
            name = prop

        while entity.get_property(name) is not None:
            entity.remove_property(name)

    @staticmethod
    # move to api?
    def set_property(entity, prop, value, datatype=None):
        AbstractCFood.remove_property(entity, prop)

        if datatype is not None:
            entity.add_property(prop, value, datatype=datatype)
        else:
            entity.add_property(prop, value)


def assure_object_is_in_list(obj, containing_object, property_name,
                             to_be_updated):
    """
    Checks whether `obj` is one of the values in the list property
    `property_name` of the supplied entity  containing_object`.

    If this is the case this function returns. Otherwise the entity is added to
    the property `property_name` and the entity `containing_object` is added to
    the supplied list to_be_updated in order to indicate, that the entity
    `containing_object` should be updated.

    If the property is missing, it is added first and then the entity is added.
    """

    if containing_object.get_property(property_name) is None:
        containing_object.add_property(property_name, value=[],
                                       datatype=db.LIST(property_name))
    current_list = containing_object.get_property(property_name).value
    contained = False

    for el in current_list:
        if el == obj:
            contained = True

            break

    if contained:
        return

    current_list.append(obj)
    to_be_updated.append(containing_object)


def assure_has_parent(entity, parent, to_be_updated):
    """
    Checks whether `entity` has a parent with name `parent`.

    If this is the case this function returns. Otherwise the entity is assigned
    a new parent and is added to the supplied list to_be_updated in order to
    indicate, that the entity `entity` should be updated.
    """
    parents = entity.get_parents()
    contained = False

    for el in parents:
        if el.name == parent:
            contained = True

            break

    if contained:
        return

    entity.add_parent(parent)
    to_be_updated.append(entity)


def insert_id_based_on_name(entity):
    if entity.name is not None and (entity.id is None or entity.id < 0):
        entity.id = get_entity(entity.name).id


def get_ids_for_entities_with_names(entities):
    for ent in entities:
        insert_id_based_on_name(ent)

        for prop in ent.get_properties():
            insert_id_based_on_name(prop)

        for parent in ent.get_parents():
            insert_id_based_on_name(parent)
            insert_id_based_on_name(ent)
