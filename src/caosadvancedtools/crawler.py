#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
""" Crawls a file structure and inserts Records into CaosDB based on what is
found.

CaosDB can automatically be filled with Records based on some file structure.
The Crawler will iterate over the files and test for each file whether a CFood
exists that matches the file path. If one does, it is instanciated to treat the
match. This occurs in basically three steps:
1. create a list of identifiables, i.e. unique representation of CaosDB Records
(such as an experiment belonging to a project and a date/time)
2. the identifiables are either found in CaosDB or they are created.
3. the identifiables are update based on the date in the file structure
"""


import traceback

import caosdb as db
from caosdb.exceptions import TransactionError

from .cache import Cache


class Crawler(object):
    def __init__(self, food, access=lambda x: x, use_cache=False):
        """
        Parameters
        ----------
        pattern : str
                  The regex pattern for matching against file names.

        use_cache : bool, optional
                    Whether to use caching (not re-inserting probably existing
                    objects into CaosDB), defaults to False.

        access : callable, optional
                 A function that takes a CaosDB path and returns a local path
        """
        self.food = food
        self.access = access
        self.report = db.Container()
        self.use_cache = use_cache

        if self.use_cache:
            self.cache = Cache()

    def crawl(self, files):
        for crawled_file in files:
            # if crawled_file.size == 0:
            #    crawled_file.add_message(
            #        type="Warning", description="This file is empty. Shouldn't we delete it?")
            #    self.report.append(crawled_file)

            #    continue

            for Cfood in self.food:
                if Cfood.match(crawled_file.path) is not None:
                    try:
                        cfood = Cfood(crawled_file, access=self.access)
                        cfood.create_identifiables()

                        if self.use_cache:
                            hashes = self.cache.update_ids_from_cache(
                                cfood.identifiables)

                        self.find_or_insert_identifiables(cfood.identifiables)

                        if self.use_cache:
                            self.cache.insert_list(hashes, cfood.identifiables)

                        cfood.update_identifiables()
                        cfood.push_identifiables_to_CaosDB()
                    except Exception as e:
                        traceback.print_exc()
                        print(e)

    @staticmethod
    def find_or_insert_identifiables(identifiables):
        """ Sets the ids of identifiables (that do not have already an id from the
        cache) based on searching CaosDB and retrieves those entities.
        The remaining entities (those which can not be retrieved) have no
        correspondence in CaosDB and are thus inserted.
        """
        # looking for matching entities in CaosDB when there is no valid id
        # i.e. there was none set from a cache

        for ent in identifiables:
            if ent.id is None or ent.id < 0:
                existing = Crawler.find_existing(ent)

                if existing is not None:
                    ent.id = existing.id

        # this makes entities with existing ids valid
        # identifiables.retrieve(unique=True, raise_exception_on_error=False)

        # insert missing, i.e. those which are not valid
        missing_identifiables = db.Container()
        missing_identifiables.extend([ent for ent in identifiables
                                      if ent.id is None or ent.id < 0])
        # TODO the following should not be necessary. Fix it

        for ent in missing_identifiables:
            ent.id = None

        missing_identifiables.insert()
        identifiables.retrieve(unique=True, raise_exception_on_error=False)

    @staticmethod
    def find_existing(entity):
        """searches for an entity that matches the identifiable in CaosDB

        Characteristics of the identifiable like, properties, name or id are
        used for the match.
        """

        if entity.name is None:
            # TODO multiple parents are ignored! Sufficient?
            query_string = "FIND Record " + entity.get_parents()[0].name
            query_string += " WITH " + " AND ".join(
                ["'" + p.name + "'='"
                 + str(get_value(p)) + "'" for p in entity.get_properties()])
        else:
            query_string = "FIND '{}'".format(entity.name)

        q = db.Query(query_string)
        # the identifiable should identify an object uniquely. Thus the query
        # is using the unique keyword
        try:
            r = q.execute(unique=True)
        except TransactionError:
            r = None

        # if r is not None:
        #     print("Found Entity with id:", r.id)
        # else:
        #     print("Did not find an existing entity.")

        return r

    @staticmethod
    def query_files(path):
        query_str = "FIND FILE WHICH IS STORED AT " + \
            (path if path.endswith("/") else path + "/") + "**"
        print("FILES QUERY: " + query_str)
        files = db.execute_query(query_str)
        print("{} FILES TO BE PROCESSED.".format(len(files)))

        return files


def get_value(prop):
    """ Returns the value of a Property

    Parameters
    ----------
    prop : The property of which the value shall be returned.

    Returns
    -------
    out : The value of the property; if the value is an entity, its ID.

    """

    if isinstance(prop.value, db.Entity):
        return prop.value.id
    else:
        return prop.value
