#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import caosdb as db

from caosadvancedtools.cfood import AbstractCFood


class ExampleCFood(AbstractCFood):
    def create_identifiables(self, crawled_file, match):
        print("create_identifiables")
        entities = {}
        entities["exp"] = db.Record()
        #import IPython
        # IPython.embed()
        entities["exp"].add_parent(name="Experiment")
        entities["exp"].add_property(name="species", value=match.group)

        return entities

    def update_identifiables(self, entities, crawled_file, match):
        entities["exp"].add_property(name="date",
                                     value=datetime.today().isoformat())
        db.Container().extend(entities.values).update()
