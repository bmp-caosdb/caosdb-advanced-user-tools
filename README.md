[![pipeline status](https://gitlab.gwdg.de/bmp-caosdb/caosdb-advanced-user-tools/badges/master/pipeline.svg)](https://gitlab.gwdg.de/bmp-caosdb/caosdb-advanced-user-tools/commits/master)

Project migrated to https://gitlab.com/caosdb

# Welcome

This is the **CaosDB Advanced User Tools** repository and a part of the
CaosDB project.

# Setup

Please read the [README_SETUP.md](README_SETUP.md) for instructions on how to
setup this code.


# Further Reading

Please refer to the [official gitlab repository of the CaosDB
project](https://gitlab.gwdg.de/bmp-caosdb/caosdb) for more information.

# License

Copyright (C) 2018 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization Göttingen.

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

