# Installation
pip install . --user
pip install tox --user

# Run Unit Tests
tox

# Code Formatting

autopep8 -i -r ./
