#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
import unittest

import caosdb as db

from caosadvancedtools.cfood import (AbstractCFood, assure_has_parent,
                                     assure_object_is_in_list)

PATTERN = "h.*"


class TestCFood(AbstractCFood):

    @staticmethod
    def get_re():
        return PATTERN


class CFoodReTest(unittest.TestCase):
    def test(self):
        self.assertEquals(TestCFood.get_re(), PATTERN)
        self.assertEqual(TestCFood._pattern, None)
        self.assertIsNotNone(TestCFood.match("hallo"))
        self.assertIsNotNone(TestCFood._pattern)
        self.assertIsNotNone(TestCFood.match("hallo"))
        self.assertIsNone(TestCFood.match("allo"))


class InsertionTest(unittest.TestCase):
    def test_contained_in_list(self):
        entity_with_list = db.Record()
        prop_name = "list_prop"
        to_be_updated = []
        assure_object_is_in_list(1005, entity_with_list, prop_name,
                                 to_be_updated)
        assert to_be_updated[0] is entity_with_list
        assert (to_be_updated[0].get_property(prop_name).value[0] == 1005)
        to_be_updated = []
        assure_object_is_in_list(1005, entity_with_list, prop_name,
                                 to_be_updated)
        assert len(to_be_updated) == 0

    def test_parent(self):
        entity = db.Record()
        to_be_updated = []
        assure_has_parent(entity, "parent", to_be_updated)
        assert to_be_updated[0] is entity
        assert (to_be_updated[0].get_parents()[0].name == "parent")
        to_be_updated = []
        assure_has_parent(entity, "parent", to_be_updated)
        assert len(to_be_updated) == 0
