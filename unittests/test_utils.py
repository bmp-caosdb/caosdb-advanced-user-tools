#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
import unittest

from caosadvancedtools.utils import (assure_absolute_path_in_glob,
                                     string_to_person)


class Assure_absoluteTest(unittest.TestCase):

    def test_abs(self):
        assert assure_absolute_path_in_glob("/hi/leute", "/Data") == "/hi/leute"

    def test_rel(self):
        assert assure_absolute_path_in_glob("hi/leute", "/Data") == "/Data/hi/leute"

    def test_compress(self):
        assert assure_absolute_path_in_glob("../leute", "/Data") == "/leute"


def is_dhornung(rec):
    if (rec.get_property("firstname").value == "Daniel"
            and rec.get_property("lastname").value == "Hornung"):

        return True

    return False


class PersonParserTest(unittest.TestCase):
    def test_simple(self):
        rec = string_to_person("Daniel Hornung")
        assert is_dhornung(rec)
        rec = string_to_person("Daniel Hornung (MPI)")
        assert is_dhornung(rec)
        rec = string_to_person("Henrik tom Wörden (MPI)")
        assert not is_dhornung(rec)
