#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
"""creates a folder structure with fake data files"""

import argparse
import numpy as np
import os
from argparse import RawTextHelpFormatter
from datetime import datetime, timedelta


def main(folder, dry=True):
    base_path = os.path.realpath(folder)
    print("Base directory:\n"+base_path)
    if not os.path.exists(base_path) and not dry:
        os.system("mkdir -p "+base_path)

    for ii, ser in enumerate(["Series_{}".format(i) for i in range(3)]):
        series_path = os.path.join(base_path, ser)
        print("Series:\n"+ser + "\n")
        if not dry:
            os.mkdir(series_path)
        for date in [datetime.today()-timedelta(days=i)-timedelta(weeks=50*ii) for i in range(10)]:
            #import IPython
            # IPython.embed()
            exp_path = os.path.join(series_path, "Exp_"+str(date.date()))
            print("Exp: "+os.path.basename(exp_path))
            if not dry:
                os.mkdir(exp_path)
            for measurement in ["Measurement_{}".format(i) for i in range(3)]:
                m_path = os.path.join(exp_path, measurement)
                print("Measurement: "+os.path.basename(m_path))
                if not dry:
                    os.mkdir(m_path)
                for sec in [np.random.randint(7000) for i in range(20)]:
                    d_path = os.path.join(
                        m_path,
                        "data_{}.dat".format(
                            (date-timedelta(seconds=sec)).isoformat()))
                    if not dry:
                        os.system("touch "+d_path)


def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=RawTextHelpFormatter)
    parser.add_argument("folder")
    parser.add_argument("-d", "--dry-run", action="store_true")

    return parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    main(args.folder, dry=args.dry_run)
